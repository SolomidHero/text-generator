import argparse
import random
import json
import sys
import subprocess


def get_parser():
    parser = argparse.ArgumentParser(description='Generate text.')
    parser.add_argument('--model',
                        type=argparse.FileType('r', encoding='UTF-8'),
                        required=True,
                        help='Read file with frequences model.')
    parser.add_argument('--seed', dest="first_word",
                        type=str, help='First word of generating text.')
    parser.add_argument('--length', type=int,
                        help='Number of words in genereted text.')
    parser.add_argument('--output',
                        type=argparse.FileType('w', encoding='UTF-8'),
                        help='Write generated text in file.')
    return parser


def set_length(num):
    if num is None:
        num = random.randint(50, 150)
    return num


def set_seed(first_word, model):
    if first_word is None:
        n = random.randint(0, len(model) - 1)
        if len(model) == 0:
            return None

        for first_word in model:
            if n == 0:
                break
            n -= 1

        n = random.randint(0, len(model[first_word]) - 1)
        for sec_word in model[first_word]:
            if n == 0:
                return (first_word, sec_word)
            n -= 1
    else:
        n = random.randint(0, len(model[first_word]) - 1)
        for sec_word in model[first_word]:
            if n == 0:
                return (first_word, sec_word)
            n -= 1


def get_rand_dict_elem(dictionary):
    amount = 0
    for key, value in dictionary.items():
        amount += value

    num = random.randint(0, amount - 1)
    for key, value in dictionary.items():
        if num < value:
            return key
        num -= value


def set_output(output):
    if output is None:
        return sys.stdout
    return output


def generate(seed, length, model, output):
    word = seed
    if word is None:
        output.write("no text generated")
        return
    else:
        output.write(word[0] + " " + word[1] + " ")

    for i in range(length):
        if not word[0] in model or not word[1] in model[word[0]]:
            break
        else:
            next_word = get_rand_dict_elem(model[word[0]][word[1]])

        output.write(next_word + " ")
        word = (word[1], next_word)
    return


def main():
    _parser = get_parser()
    args = _parser.parse_args()
    _model = json.load(args.model)
    _length = set_length(args.length)
    _seed = set_seed(args.first_word, _model)
    _output = set_output(args.output)

    generate(_seed, _length, _model, _output)

if __name__ == '__main__':
    main()
