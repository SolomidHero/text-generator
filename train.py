import argparse
import json
import sys
import os
import re
from collections import defaultdict


def get_parser():
    parser = argparse.ArgumentParser(description='Train generator.')
    parser.add_argument(
        '--input-dir', dest='input', type=str, help='Read file with text.')
    parser.add_argument(
        '--model',
        type=argparse.FileType('w', encoding='UTF-8'),
        required=True,
        help='Write model based on frequencies.')
    return parser


def get_args(parser):
    return parser.parse_args()


def set_input(_input):
    if _input is None:
        return ([sys.stdin])
    return get_path(_input)


def get_path(_input):
    read_files = []
    for path, dirs, files in os.walk(_input):
        for file in files:
            if file.endswith(".txt"):
                read_files.append(open(os.path.join(path, file)))
    return read_files


def make_model(_input):
    model = dict()
    model = defaultdict(lambda: "", model)
    for file in _input:
        prev_word = ("", "")
        with file:
            for line in file:
                words = re.findall("[a-zA-Z0-9_',:;!?.]+", line)
                print (words)
                if not words:
                    continue

                for i in range(len(words)):
                    if words[i][-1] == ':' or words[i][-1] == ';'\
                            or words[i][-1] == ',' or words[i][-1] == '\n':
                        words[i] = words[i][:-1:]

                for i in range(len(words) - 2):
                    model[words[i]] = defaultdict(lambda: "", model[words[i]])
                    model[words[i]][words[i + 1]] = \
                        defaultdict(lambda: 0, model[words[i]][words[i + 1]])
                    model[words[i]][words[i + 1]][words[i + 2]] += 1

                model[prev_word[0]] = defaultdict(lambda: "", model[prev_word[0]])
                model[prev_word[0]][prev_word[1]] = \
                    defaultdict(lambda: 0, model[prev_word[0]][prev_word[1]])
                model[prev_word[0]][prev_word[1]][words[0]] += 1

                if len(words) > 1:
                    model[prev_word[1]] = \
                        defaultdict(lambda: "", model[prev_word[1]])
                    model[prev_word[1]][words[0]] = \
                        defaultdict(lambda: 0, model[prev_word[1]][words[0]])
                    model[prev_word[1]][words[0]][words[1]] += 1

                prev_word = \
                    (prev_word[1] if len(words) == 1 else words[-2], words[-1])
    return model


def main():
    _parser = get_parser()
    _args = get_args(_parser)
    _input = set_input(_args.input)
    _args.model.write(json.dumps(
        make_model(_input), indent=2, d))


if __name__ == '__main__':
    main()
